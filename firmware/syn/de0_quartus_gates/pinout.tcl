
post_message "Assigning pinout"

# Load Quartus II Tcl Project package
package require ::quartus::project

project_open -revision demo demo

# Pin assignments for the input
set_location_assignment PIN_H2 -to btn0_i
set_location_assignment PIN_J6 -to sw0_i
set_location_assignment PIN_H5 -to sw1_i
set_location_assignment PIN_H6 -to sw2_i

# Pin assignments for the output
set_location_assignment PIN_J1 -to led0_o
set_location_assignment PIN_J2 -to led1_o
set_location_assignment PIN_J3 -to led2_o

# Commit assignments
export_assignments
project_close



