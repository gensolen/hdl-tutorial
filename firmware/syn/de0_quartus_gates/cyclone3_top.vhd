-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file cyclone3_top.vhd
--! @brief top module
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cyclone3_top is
  port (
    sw0_i   : in  std_logic;
    sw1_i   : in  std_logic;
    sw2_i   : in  std_logic;
    btn0_i  : in  std_logic;
    led0_o  : out std_logic;
    led1_o  : out std_logic;
    led2_o  : out std_logic
  );
end cyclone3_top;

----------------------------------------------------------------------

architecture arch of cyclone3_top is
begin

  gates_inst: entity work.gates
  port map (
    sw0_i   => sw0_i,
    sw1_i   => sw1_i,
    sw2_i   => sw2_i,
    btn0_i  => not btn0_i,
    led0_o  => led0_o,
    led1_o  => led1_o,
    led2_o  => led2_o
  );

end arch;
