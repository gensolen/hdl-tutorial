
target = "altera"
action = "synthesis"

syn_device = "ep3c16"
syn_grade = "c6"
syn_package = "f484"
syn_top = "seconds"
syn_project = "demo"
syn_tool = "quartus"

quartus_preflow = "./pinout.tcl"

modules = {
  "local" : [ 
    "../../modules/ssd",
  ]
}

files = [
  "constraints.sdc",
]
