
post_message "Assigning pinout"

# Load Quartus II Tcl Project package
package require ::quartus::project

project_open -revision demo demo

# Pin assignments for 50 MHz oscillators
set_location_assignment PIN_G21 -to clk

# Seven Segment Digit 1
set_location_assignment PIN_E11 -to SS_D1_o[0] 
set_location_assignment PIN_F11 -to SS_D1_o[1] 
set_location_assignment PIN_H12 -to SS_D1_o[2] 
set_location_assignment PIN_H13 -to SS_D1_o[3] 
set_location_assignment PIN_G12 -to SS_D1_o[4] 
set_location_assignment PIN_F12 -to SS_D1_o[5] 
set_location_assignment PIN_F13 -to SS_D1_o[6] 
set_location_assignment PIN_D13 -to SS_D1P_o

# Commit assignments
export_assignments
project_close



