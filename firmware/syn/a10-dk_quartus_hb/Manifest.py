
target = "altera"
action = "synthesis"

# FPGA on devkit: 10AX115S2F45I1SG
syn_family = "Arria 10"
syn_device = "10ax115s2"
syn_package = "f45"
syn_grade = "i1sg"

syn_top = "breadboard"
syn_project = "demo"
syn_tool = "quartus"

# quartus_preflow = "pinout.tcl"
quartus_preflow = "setup.tcl"
quartus_postmodule = "module.tcl"

modules = {
  "local" : [ 
#    "../_targets/a10_dk",
#    '../../modules/PoC_lib',
    '../../modules/heartbeat'
  ],
}

files = [
  "breadboard.vhd"
]

