
post_message "Assigning pinout"

# Load Quartus II Tcl Project package
# package require ::quartus::project

#project_open -revision demo demo

# Pin assignments for main 100 MHz oscillator (differential signal))
set_location_assignment PIN_F34 -to clock_100_p_i
set_location_assignment PIN_F35 -to clock_100_n_i

# red user LEDs
set_location_assignment PIN_L27 -to led_r_o[0]
set_location_assignment PIN_J26 -to led_r_o[1]
set_location_assignment PIN_K24 -to led_r_o[2]
set_location_assignment PIN_L23 -to led_r_o[3]
set_location_assignment PIN_B20 -to led_r_o[4]
set_location_assignment PIN_C19 -to led_r_o[5]
set_location_assignment PIN_D19 -to led_r_o[6]
set_location_assignment PIN_M23 -to led_r_o[7]

# green user LEDs
set_location_assignment PIN_L28 -to led_g_o[0]
set_location_assignment PIN_K26 -to led_g_o[1]
set_location_assignment PIN_K25 -to led_g_o[2]
set_location_assignment PIN_L25 -to led_g_o[3]
set_location_assignment PIN_J24 -to led_g_o[4]
set_location_assignment PIN_A19 -to led_g_o[5]
set_location_assignment PIN_C18 -to led_g_o[6]
set_location_assignment PIN_D18 -to led_g_o[7]

# user buttons
set_location_assignment PIN_T12 -to button_i[0]
set_location_assignment PIN_U12 -to button_i[1]
set_location_assignment PIN_U11 -to button_i[2]

# Commit assignments
#export_assignments
#project_close


