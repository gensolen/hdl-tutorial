
# Load Quartus II Tcl Project package
package require ::quartus::project

project_open -revision demo demo

post_message "  *** Assigning pinout ***"
source [file join [file dirname [info script]] "pinout.tcl"]

post_message "  *** Assigning SignalTap ***"
source [file join [file dirname [info script]] "settings.tcl"]

# post_message "  *** Assigning SignalTap ***"
# source [file join [file dirname [get_project_directory]] "signalTap.tcl"]

# Commit assignments
export_assignments
project_close

