----------------------------------------------------------------------
-- Design  : 
-- Author  : 
----------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

-- library mylib;  -- heartbeat

library work;

entity breadboard is
  port (
    clock_100_p_i : in  std_logic;
    led_r_o       : out std_logic_vector(7 downto 0) := (others => '1'); -- negative logic
    led_g_o       : out std_logic_vector(7 downto 0) := (others => '1')  -- negative logic
  );
end breadboard;


----------------------------------------------------------------------

architecture rtl of breadboard is

--  component heartbeat                      -- a 32 bit counter
--    generic (
--      low_bit  : natural;
--      high_bit : natural
--    );
--    port (
--      clk:  in std_logic;
--      led: out std_logic_vector(high_bit - low_bit downto 0) 
--    );
--  end component;

  constant NUM_LED      : positive := 8;  -- number of output LEDs (per color)

  constant HB_LOW_BIT  : positive := 26; -- select bit of heartbeat counter to be displayed (-> blink frequency)
  constant HB_HIGH_BIT : positive := 26;

  signal s_clock : std_logic;
  signal s_led : std_logic_vector(HB_HIGH_BIT - HB_LOW_BIT downto 0);

begin

--  m_hb : heartbeat                -- works with component definition
  m_hb : entity work.heartbeat    -- works without component
--  m_hb : entity mylib.heartbeat   -- doesn't work with hdlmake 3.0, "will-hacks-July18" patch needed
    generic map (
      low_bit  => HB_LOW_BIT,
      high_bit => HB_HIGH_BIT
    )
    port map (
      clk => s_clock, 
      led => s_led
    );

  s_clock <= clock_100_p_i;

  led_r_o(HB_HIGH_BIT - HB_LOW_BIT downto 0) <= not s_led;                     -- Note: negative logic
  led_r_o(NUM_LED - 1 downto HB_HIGH_BIT - HB_LOW_BIT + 1) <= (others => '1'); -- switch unused leds off


  led_g_o(NUM_LED - 1 downto 0) <= (others => '1');                            -- switch leds off

end architecture rtl;

-----------------------------------------------------------------------

