-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file cyclone3_top.vhd
--! @brief top module
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cyclone3_top is
  generic (
    OFFSET      : positive := 1024;
    CTRL_BIT    : natural  := 1;
    UART_DBIT   : positive := 8;    -- uart data bits in uart communication
    SAMPLES_MAX : positive := 32;
    SIMULATE    : boolean  := false;
    bit_width   : positive := 14;
    invert      : boolean  := true
  );
  port (
    clk : in std_logic;

    button_i  : in std_logic;
    btn_rst   : in std_logic;

    led_o : out std_logic_vector(1 downto 0) := (others => '0');

    ss_d1_o : out std_logic_vector(6 downto 0) := (others => '0');
    ss_d2_o : out std_logic_vector(6 downto 0) := (others => '0');
    ss_d3_o : out std_logic_vector(6 downto 0) := (others => '0');
    ss_d4_o : out std_logic_vector(6 downto 0) := (others => '0');

    uart_rxd_i : in std_logic;          -- receive data
    uart_txd_o : out std_logic          -- transmit data
  );
end cyclone3_top;

----------------------------------------------------------------------

architecture arch of cyclone3_top is

begin

  const_inst: entity work.top
  generic map (
    OFFSET      => OFFSET,
    CTRL_BIT    => CTRL_BIT,
    UART_DBIT   => UART_DBIT,
    SAMPLES_MAX => SAMPLES_MAX,
    SIMULATE    => SIMULATE,
    bit_width   => bit_width,
    invert      => invert
  )
  port map (
    clk         => clk,
    button_i    => not button_i,
    btn_rst     => not btn_rst,
    led_o       => led_o,
    ss_d1_o     => ss_d1_o,
    ss_d2_o     => ss_d2_o,
    ss_d3_o     => ss_d3_o,
    ss_d4_o     => ss_d4_o,
    uart_rxd_i  => uart_rxd_i,
    uart_txd_o  => uart_txd_o
  );

end arch;
