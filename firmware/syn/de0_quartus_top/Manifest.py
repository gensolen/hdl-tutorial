
target = "altera"
action = "synthesis"

syn_device = "ep3c16"
syn_grade = "c6"
syn_package = "f484"
syn_top = "cyclone3_top"
syn_project = "demo"
syn_tool = "quartus"

quartus_preflow = "./setup.tcl"

modules = {
  "local" : [ 
    "../../modules/top",
  ]
}

files = [
  "cyclone3_top.vhd",
  "constraints.sdc",
  "stp1.stp",
]
