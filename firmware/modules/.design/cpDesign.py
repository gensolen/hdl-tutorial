

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys 
from subprocess import Popen, PIPE, call


# tuple format: (fileName, targetDir)
# Note: the file names are relative to the current folder ./,
#       the target dirctory is relative to the parent directory ../
fList = [
    ("filter.vhd", "filter"),
    ("gates.vhd", "gates"),
    ("lohi_detect.vhd", "lohi_detect"),
    ("max_find.vhd", "max_find"),
    ("seconds.vhd", "ssd"),
    ("ssd.vhd", "ssd"),
    ("top.vhd", "top")
  ]

def spawnCommand(command):
  pipe = Popen(command, stdin=PIPE)
  returncode = pipe.wait()
  assert returncode == 0


def main(args):
  for fileTuple in fList:
    command = [] 
    command.append('cp') 
    command.append('./' + fileTuple[0]) 
    command.append('../' + fileTuple[1])
    print(' ', ' '.join(command))
    spawnCommand(command)


if __name__ == "__main__":
  main(sys.argv[1:])

