-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file seconds.vhd
--! @brief displays a seconds counter on one seven segment display (ssd)
--! @author: Philipp Horn <philipp.horn@cern.ch>
--! @details: This module uses single_disp and a 50 MHz clock to count and
--!           display a second counter
--------------------------------------------------------------------------------

library IEEE;
use     IEEE.std_logic_1164.ALL;
use     IEEE.numeric_std.ALL;

entity seconds is
  generic (
    counter_max : positive := 50_000_000;
    invert    : boolean := true
  );
  port (
    clk : in  std_logic;
    ss_d1_o : out std_logic_vector(6 downto 0) := (others => '0')
  );
end seconds;

architecture arch of seconds is

  -- increased with every second
  signal sec:
  -- increased with every rising clock
  signal counter:


begin


end arch;
