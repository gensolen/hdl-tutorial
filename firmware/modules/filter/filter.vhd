-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file filter.vhd
--! @brief simple filter
--! @author: Philipp Horn <philipp.horn@cern.ch>
--! @details: This module implements a filter.
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity filter is
  generic (
    -- signals, which are declared here can be used in the port definition
    bit_width : positive := 14
  );
  port (
    clk     : in  std_logic;
    data_i  : in  signed(bit_width-1 downto 0);
    data_o  : out signed(bit_width-1 downto 0) := (others => '0')
  );
end entity;

architecture arch of filter is

  -- procedure to avoid calculation with fixed comma digits:
    -- constant coefficients are multiplied with 2^n to get integer values
    -- lower n bits of the result are omitted (same as division by 2^n)

  -- number of bits the coefficients were shifted to the right
  -- the result has to be shifted to the left by the same amount at the end
  constant shif : natural := 15;

  type const_t is array (natural range <>) of integer;
  constant c : const_t(1 to 6) := (
    -9379,  -- c(1)
    17867,  -- c(2)
    44585,  -- c(3)
    -68495, -- c(4)
    36820,  -- c(5)
    -8483   -- c(6)
  );

  signal data_uns : signed(bit_width+shif-1 downto 0) := (others => '0');

  signal xn   : integer := 0;
  signal xn_1 : integer := 0;
  signal xn_2 : integer := 0;
  signal xn_3 : integer := 0;
  signal xn_4 : integer := 0;
  signal xn_5 : integer := 0;
  signal yn   : integer := 0;

--  signal x : const_t(0 to 6) := (others => 0);

begin

  xn <= to_integer(data_i);

  fir_proc: process(clk)
  begin
    if rising_edge(clk) then
      xn_1 <= xn;
      xn_2 <= xn_1;
      xn_3 <= xn_2;
      xn_4 <= xn_3;
      xn_5 <= xn_4;
      yn <= c(1) * xn + c(2) * xn_1 + c(3) * xn_2 + c(4) * xn_3 + c(5) * xn_4 + c(6) * xn_5;

--      for i in 0 to 5 loop
--        x(i) <= c(i+1) * xn + x(i+1);
--      end loop;

    end if;
  end process fir_proc;

  data_uns <= to_signed(yn,bit_width+shif);

--  yn   <= x(0);

  -- only the bit_width highest bits are sent to the output
  data_o <= data_uns(bit_width+shif-1 downto shif);

end arch;
