-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file seconds.vhd
--! @brief displays a seconds counter on one seven segment display (ssd)
--! @author: Philipp Horn <philipp.horn@cern.ch>
--! @details: This module uses single_disp and a 50 MHz clock to count and
--!           display a second counter
--------------------------------------------------------------------------------

library IEEE;
use     IEEE.std_logic_1164.ALL;
use     IEEE.numeric_std.ALL;

entity seconds is
  generic (
    counter_max : positive := 50000000;
    invert    : boolean := true
  );
  port (
    clk : in  std_logic;
    ss_d1_o : out std_logic_vector(6 downto 0) := (others => '0')
  );
end seconds;

architecture arch of seconds is

  -- increased with every second
  signal sec: natural := 0;
  -- increased with every rising clock
  signal counter: positive := 1;

  signal sec_uns: signed(4 downto 0) := (others => '0');

begin

  sec_uns <= to_signed(sec, sec_uns'length);
    
  map_ssd: entity work.single_disp
    generic map(
      invert => invert
      )
    port map(
      number_i => sec_uns,
      seg_o => ss_d1_o
      );

  process_seconds : process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
      if (counter = counter_max) then
        if (sec = 9) then
          sec <= 0;
        else
          sec <= sec + 1;
        end if;
        counter <= 1;             
      end if;
    end if;
                
  end process process_seconds;

end arch;
