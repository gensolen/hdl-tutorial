onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /top_tb/clk
add wave -noupdate -color Yellow -label button_i /top_tb/button_i
add wave -noupdate -color Yellow -label start /top_tb/uut/lohi_detect_inst/sig_o
add wave -noupdate -color Red -label data_raw -radix decimal /top_tb/uut/filter_inst/data_i
add wave -noupdate -color Red -label data_filter -radix decimal /top_tb/uut/filter_inst/data_o
add wave -noupdate -color Red -label data_max -radix decimal /top_tb/uut/max_find_inst/data_o
add wave -noupdate -color Cyan -label ss_d1_o /top_tb/uut/ss_d1_o
add wave -noupdate -color Cyan -label ss_d2_o /top_tb/uut/ss_d2_o
add wave -noupdate -color Cyan -label ss_d3_o /top_tb/uut/ss_d3_o
add wave -noupdate -color Cyan -label ss_d4_o /top_tb/uut/ss_d4_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 124
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {210 ns}
run 200 ns
