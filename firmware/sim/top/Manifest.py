action = "simulation"
sim_tool = "modelsim"
sim_top = "top_tb"

sim_post_cmd = "vsim -do wave.do -i top_tb"

files = [
  "top_tb.vhd"
]

modules = {
  "local" : [ "../../modules/top" ],
}
