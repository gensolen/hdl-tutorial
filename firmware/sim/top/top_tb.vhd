-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file top_tb.vhd
--! @brief Testbench, which simulates top.vhd
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top_tb is
  -- Define generic for uut (unit under test)
  generic (
    OFFSET      : positive := 1024;
    CTRL_BIT    : natural  := 1;
    UART_DBIT   : positive := 8;    -- uart data bits in uart communication
    SAMPLES_MAX : positive := 32;
    SIMULATE    : boolean  := true;
    bit_width   : positive := 14;
    invert      : boolean  := true
  );
end top_tb;

architecture tb of top_tb is

  -- define general signals
  signal clk      : std_logic := '0';

  -- define signals for uut ports
  signal button_i : std_logic := '0';
  signal ss_d1_o : std_logic_vector(6 downto 0) := (others => '0');
  signal ss_d2_o : std_logic_vector(6 downto 0) := (others => '0');
  signal ss_d3_o : std_logic_vector(6 downto 0) := (others => '0');
  signal ss_d4_o : std_logic_vector(6 downto 0) := (others => '0');

begin

  -- uut top.vhd
  uut: entity work.top
  generic map (
    OFFSET      => OFFSET,
    CTRL_BIT    => CTRL_BIT,
    UART_DBIT   => UART_DBIT,
    SAMPLES_MAX => SAMPLES_MAX,
    SIMULATE    => SIMULATE,
    bit_width   => bit_width,
    invert      => invert
  )
  port map (
    clk         => clk,
    button_i    => button_i,
    btn_rst     => '0',
    led_o       => open,
    ss_d1_o     => ss_d1_o,
    ss_d2_o     => ss_d2_o,
    ss_d3_o     => ss_d3_o,
    ss_d4_o     => ss_d4_o,
    uart_rxd_i  => '0',
    uart_txd_o  => open
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- define input for simulation
  sim_proc: process
  begin
    wait for 12 ns;
    button_i <= '1';
    wait for 81 ns;
    button_i <= '0';
    wait;
  end process sim_proc;
end tb;
