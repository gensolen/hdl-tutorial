onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Yellow -label sw0_i /gates_tb/sw0_i
add wave -noupdate -color Yellow -label sw1_i /gates_tb/sw1_i
add wave -noupdate -color Yellow -label sw2_i /gates_tb/sw2_i
add wave -noupdate -color Yellow -label btn0_i /gates_tb/btn0_i
add wave -noupdate -color Cyan -label led0_o /gates_tb/led0_o
add wave -noupdate -color Cyan -label led1_o /gates_tb/led1_o
add wave -noupdate -color Cyan -label led2_o /gates_tb/led2_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 91
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {73500 ps}
run 70 ns
