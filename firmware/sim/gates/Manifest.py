action = "simulation"
sim_tool = "modelsim"
sim_top = "gates_tb"

sim_post_cmd = "vsim -do wave.do -i gates_tb"

files = [
  "gates_tb.vhd"
]

modules = {
  "local" : [ "../../modules/gates" ],
}
