-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file gates_tb.vhd
--! @brief Testbench, which simulates gates
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity gates_tb is
end gates_tb;

architecture arch of gates_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut (unit under test) ports
  signal sw0_i  : std_logic := '0';
  signal sw1_i  : std_logic := '0';
  signal sw2_i  : std_logic := '0';
  signal btn0_i : std_logic := '0';
  signal led0_o : std_logic := '0';
  signal led1_o : std_logic := '0';
  signal led2_o : std_logic := '0';

begin

  -- uut gates.vhd
  gates_inst: entity work.gates
  port map (
    sw0_i   => sw0_i,
    sw1_i   => sw1_i,
    sw2_i   => sw2_i,
    btn0_i  => btn0_i,
    led0_o  => led0_o,
    led1_o  => led1_o,
    led2_o  => led2_o
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- generate a counter to time input
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;

  -- define input for simulation
  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      case counter is
      when 1 =>
        sw0_i   <= '1';
        sw1_i   <= '0';
        sw2_i   <= '0';
        btn0_i  <= '0';
      when 2 =>
        sw0_i   <= '1';
        sw1_i   <= '1';
        sw2_i   <= '0';
        btn0_i  <= '0';
      when 3 =>
        sw0_i   <= '1';
        sw1_i   <= '1';
        sw2_i   <= '1';
        btn0_i  <= '0';
      when 4 =>
        sw0_i   <= '0';
        sw1_i   <= '1';
        sw2_i   <= '1';
        btn0_i  <= '1';
      when 5 =>
        sw0_i   <= '0';
        sw1_i   <= '0';
        sw2_i   <= '1';
        btn0_i  <= '1';
      when others =>
        sw0_i   <= '0';
        sw1_i   <= '0';
        sw2_i   <= '0';
        btn0_i  <= '0';
      end case;
    end if;
  end process sim_proc;

end arch;
