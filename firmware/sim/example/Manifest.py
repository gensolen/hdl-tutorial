action = "simulation"
sim_tool = "modelsim"
sim_top = "example_tb"

sim_post_cmd = "vsim -do wave.do -i example_tb"

files = [
  "example_tb.vhd"
]

modules = {
  "local" : [ "../../modules/example" ],
}
