-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file example_tb.vhd
--! @brief Testbench, which simulates examples
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity example_tb is
end example_tb;

architecture arch of example_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut (unit under test) ports
  signal input1 : std_logic := '0';
  signal input2 : std_logic := '0';
  signal output : std_logic := '0';

begin

  -- uut example.vhd
  example_inst: entity work.example
  port map (
    input1  => input1,
    input2  => input2,
    output  => output
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- generate a counter to time input
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;

  -- define input for simulation
  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      case counter is
      when 1 =>
        input1 <= '1';
        input2 <= '0';
      when 2 =>
        input1 <= '0';
        input2 <= '1';
      when 3 =>
        input1 <= '1';
        input2 <= '1';
      when others =>
        input1 <= '0';
        input2 <= '0';
      end case;
    end if;
  end process sim_proc;

end arch;
