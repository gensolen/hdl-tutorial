action = "simulation"
sim_tool = "modelsim"
sim_top = "seconds_tb"

sim_post_cmd = "vsim -do wave.do -i seconds_tb"

files = [
  "seconds_tb.vhd"
]

modules = {
  "local" : [ "../../modules/ssd" ],
}
