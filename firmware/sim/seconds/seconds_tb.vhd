-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file seconds_tb.vhd
--! @brief Testbench, which simulates seconds
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity seconds_tb is
  generic(
  -- Define generic for uut (unit under test)
    counter_max : positive := 5;
    invert      : boolean := true
  );
end seconds_tb;

architecture tb of seconds_tb is

  -- define general signals
  signal clk      : std_logic := '0';

begin

  -- uut ssd.vhd
  uut: entity work.seconds
  generic map(
    counter_max => counter_max,
    invert      => invert
  )
  port map(
    clk     => clk,
    ss_d1_o => open
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 20 ns
    wait for 10 ns;
    clk <= '1';
    wait for 10 ns;
  end process clk_proc;
end architecture;
