onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /seconds_tb/clk
add wave -noupdate -color Yellow -label counter /seconds_tb/uut/counter
add wave -noupdate -color Red -label sec -radix decimal /seconds_tb/uut/sec
add wave -noupdate -color Red -label ss_d1_o /seconds_tb/uut/ss_d1_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 99
configure wave -valuecolwidth 62
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1071 ns}
run 1020 ns
