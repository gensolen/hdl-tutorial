action = "simulation"
sim_tool = "modelsim"
sim_top = "ssd_tb"

sim_post_cmd = "vsim -do wave.do -i ssd_tb"

files = [
  "ssd_tb.vhd"
]

modules = {
  "local" : [ "../../modules/ssd" ],
}
