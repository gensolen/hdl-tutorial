onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /ssd_tb/clk
add wave -noupdate -color Yellow -label data_i -radix decimal /ssd_tb/data_i
add wave -noupdate -label quotient -radix decimal -childformat {{/ssd_tb/uut/quotient(4) -radix decimal} {/ssd_tb/uut/quotient(3) -radix decimal} {/ssd_tb/uut/quotient(2) -radix decimal} {/ssd_tb/uut/quotient(1) -radix decimal} {/ssd_tb/uut/quotient(0) -radix decimal}} -expand -subitemconfig {/ssd_tb/uut/quotient(4) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/quotient(3) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/quotient(2) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/quotient(1) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/quotient(0) {-color Yellow -height 15 -radix decimal}} /ssd_tb/uut/quotient
add wave -noupdate -label remainder -radix decimal -childformat {{/ssd_tb/uut/remainder(3) -radix decimal} {/ssd_tb/uut/remainder(2) -radix decimal} {/ssd_tb/uut/remainder(1) -radix decimal} {/ssd_tb/uut/remainder(0) -radix decimal}} -expand -subitemconfig {/ssd_tb/uut/remainder(3) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/remainder(2) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/remainder(1) {-color Yellow -height 15 -radix decimal} /ssd_tb/uut/remainder(0) {-color Yellow -height 15 -radix decimal}} /ssd_tb/uut/remainder
add wave -noupdate -color Red -label ss_d4_o /ssd_tb/ss_d4_o
add wave -noupdate -color Red -label ss_d3_o /ssd_tb/ss_d3_o
add wave -noupdate -color Red -label ss_d2_o /ssd_tb/ss_d2_o
add wave -noupdate -color Red -label ss_d1_o /ssd_tb/ss_d1_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 111
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {30000 ps}
run 30 ns
