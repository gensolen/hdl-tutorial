-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file ssd_tb.vhd
--! @brief Testbench, which simulates ssd
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ssd_tb is
  generic(
  -- Define generic for uut (unit under test)
    bit_width : positive := 14;
    invert : boolean := true
  );
end ssd_tb;

architecture tb of ssd_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut ports
  signal data_i  : signed(bit_width-1 downto 0) := (others => '0');
  signal ss_d1_o : std_logic_vector(6 downto 0) := (others => '0');
  signal ss_d2_o : std_logic_vector(6 downto 0) := (others => '0');
  signal ss_d3_o : std_logic_vector(6 downto 0) := (others => '0');
  signal ss_d4_o : std_logic_vector(6 downto 0) := (others => '0');

begin

  -- uut ssd.vhd
  uut: entity work.ssd
  generic map(
    bit_width => bit_width,
    invert => invert
  )
  port map(
    clk     => clk,
    data_i  => data_i,
    ss_d1_o => ss_d1_o,
    ss_d2_o => ss_d2_o,
    ss_d3_o => ss_d3_o,
    ss_d4_o => ss_d4_o
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- generate a counter to time input
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;

  -- define input for simulation
  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      case counter is
      when 1 =>
        data_i <= to_signed(7352,bit_width);
      when others =>
        data_i <= (others => '0');
      end case;
    end if;
  end process;
end architecture;
