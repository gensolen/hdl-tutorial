-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file lohi_detect_tb.vhd
--! @brief Testbench, which simulates lohi_detect.vhd
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity lohi_detect_tb is
end lohi_detect_tb;

architecture tb of lohi_detect_tb is

  -- define general signals
  signal clk      : std_logic := '0';

  -- define signals for uut (unit under test) ports
  signal sig_i  : std_logic := '0';
  signal sig_o  : std_logic := '0';

begin

  -- uut lohi_detect.vhd
  lohi_detect_inst: entity work.lohi_detect
  port map (
    clk   => clk,
    sig_i => sig_i,
    sig_o => sig_o
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- define input for simulation
  sim_proc: process
  begin
    wait for 42 ns;
    sig_i <= '1';
    wait for 34 ns;
    sig_i <= '0';
    wait;
  end process sim_proc;
end tb;
