action = "simulation"
sim_tool = "modelsim"
sim_top = "lohi_detect_tb"

sim_post_cmd = "vsim -do wave.do -i lohi_detect_tb"

files = [
  "lohi_detect_tb.vhd"
]

modules = {
  "local" : [ "../../modules/lohi_detect" ],
}
