onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /lohi_detect_tb/clk
add wave -noupdate -color Yellow -label sig_i /lohi_detect_tb/sig_i
add wave -noupdate -color Yellow -label sig_o /lohi_detect_tb/sig_o
add wave -noupdate -color Red -label reg /lohi_detect_tb/lohi_detect_inst/reg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 94
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {105 ns}
run 100 ns
