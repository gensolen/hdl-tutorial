action = "simulation"
sim_tool = "modelsim"
sim_top = "bin_add_tb"

sim_post_cmd = "vsim -do wave.do -i bin_add_tb"

files = [
  "bin_add_tb.vhd",
]

modules = {
  "local" : [ "../../modules/bin_add" ],
}
