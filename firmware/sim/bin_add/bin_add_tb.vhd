-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file bin_add_tb.vhd
--! @brief Testbench, which simulates bin_adds
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity bin_add_tb is
  generic(
    bit_length : integer := 10
  );
end bin_add_tb;

architecture arch of bin_add_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut (unit under test) ports
  signal a : std_logic_vector (bit_length-1 downto 0) := (others => '0');
  signal b : std_logic_vector (bit_length-1 downto 0) := (others => '0');

begin

  -- uut bin_add.vhd
  bin_add_inst: entity work.bin_add
  port map (
    clk => clk,
    a   => a,
    b   => b,
    c   => open
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- generate a counter to time input
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;

  -- define input for simulation
  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      case counter is
      when 1 =>
        a <= "1011100101"; --741
        b <= "0011111001"; --249
      when 2 =>
        a <= (others => '1');
        b <= (others => '1');
      when others =>
        a <= (others => '0');
        b <= (others => '0');
      end case;
    end if;
  end process sim_proc;

end arch;
