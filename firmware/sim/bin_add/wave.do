onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /bin_add_tb/clk
add wave -noupdate -color Yellow -label a -radix unsigned /bin_add_tb/a
add wave -noupdate -color Yellow -label b -radix unsigned /bin_add_tb/b
add wave -noupdate -color Yellow -label c -radix unsigned /bin_add_tb/bin_add_inst/c
add wave -noupdate -color Red -label c_temp /bin_add_tb/bin_add_inst/c_temp
add wave -noupdate -color Red -label m /bin_add_tb/bin_add_inst/m
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {95000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 90
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {52500 ps}
run 50 ns
