action = "simulation"
sim_tool = "modelsim"
sim_top = "filter_tb"

sim_post_cmd = "vsim -do wave.do -i filter_tb"

files = [
  "filter_tb.vhd"
]

modules = {
  "local" : [ 
    "../../modules/filter",
    "../../modules/injector"
  ],
}
