-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file filter_tb.vhd
--! @brief Testbench, which simulates filter.vhd
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity filter_tb is
  -- Define generic for uut (unit under test)
  generic (
    bit_width : positive := 14
  );
end filter_tb;

architecture tb of filter_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut ports
  signal sig_i  : std_logic := '0';
  signal data_i : signed(bit_width-1 downto 0) := (others => '0');

begin

  -- uut filter.vhd
  uut: entity work.filter
  generic map (
    bit_width => bit_width
  )
  port map (
    clk     => clk,
    data_i  => data_i,
    data_o  => open
  );

  -- generate input
  data_inst: entity work.data_const
  generic map (
    bit_width => bit_width
  )
  port map (
    clk     => clk,
    sig_i   => sig_i,
    data_o  => data_i
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- generate a counter to time input
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;

  -- define input for simulation
  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      if counter = 1 then
        sig_i <= '1';
      else
        sig_i <= '0';
      end if;
    end if;
  end process sim_proc;
end tb;
