

library IEEE;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;


entity getData_tb is
  generic (
    OFFSET      : positive := 1024;
    UART_FIFO_W : positive := 2;  -- uart fifo width; words in FIFO = 2**FIFO_W
    UART_DBIT   : positive := 8   -- data bits in uart communication
  );
end getData_tb;


architecture tb of getData_tb is

  constant HI_LO_BIT     : natural := 1;
  constant ADC_BIT_WIDTH : natural := 2 * (UART_DBIT - HI_LO_BIT); -- = 14

  signal clk      : std_logic := '0';
  signal rst      : std_logic := '1';
  signal counter  : natural   := 0;

	signal start  : std_logic := '0';
  signal leds   : std_logic_vector(1 downto 0);

  -- data to and from uart unit
  signal data_rx  : std_logic_vector(UART_DBIT - 1 downto 0) := (others => '0');
  signal data_tx  : std_logic_vector(UART_DBIT - 1 downto 0) := (others => '0');

  -- uart signals
  signal wr_uart   : std_logic := '0';
  signal frame_err : std_logic := '0';
  signal valid_rx  : std_logic := '0';

  -- serial uart signal lines (looped back)
  signal UART_TXD_o  : std_logic := '0';
  signal UART_RXD_i  : std_logic := '0';

  -- ADC data out and associated tick signal for flagging newly available data
  signal data_out_valid : std_logic := '0';
  signal data_out       : signed(ADC_BIT_WIDTH - 1 downto 0);

begin

  -- hold reset high up for some time (1 us)
  rst <= '1' when counter < 500 else
         '0';


  -- loopback serial uart lines
  UART_RXD_i <= UART_TXD_o;


  -- generate a 50 MHz clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 20 ns (= 50 MHz)
    wait for 10 ns;
    clk <= '1';
    wait for 10 ns;
  end process clk_proc;


  -- generate a time counter (clock cycles)
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;


  ---------------------------------------
  --  data injector (via uart loopback)
  ---------------------------------------

  sim_proc : process(clk)
  begin
    if rising_edge(clk) then
			start <= '0';
      case counter is
        when 100000 =>
          wr_uart <= '1';
          data_tx <= x"0f";   -- 0000_1111
        when 300000 =>
          wr_uart <= '1';     -- x'ef' = b'1110_1111'
          data_tx <= x"ef";   -- should result in 11_0111_1000_1111
        when 500000 =>
          wr_uart <= '1';
          data_tx <= x"70";   -- 0111_0000
        when 700000 =>
          wr_uart <= '1';     -- x'a1' = b'1010_0001'
          data_tx <= x"a1";   -- should result in 01_0000_1111_0000
				when 800000 =>
					start <= '1';
        when others =>
          wr_uart <= '0';
          data_tx <= (others => '0');
      end case;
    end if;
  end process sim_proc;
  ---------------------------------------


  -- unit under test
  uut : entity work.getData
  generic map (
    OFFSET      => OFFSET,
    BIT_WIDTH   => ADC_BIT_WIDTH,
		SAMPLES_MAX => 2
  )
  port map (
    clk   => clk,
    rst   => rst,

		start => start,
		leds  => leds,

    -- data in
    data_in_frame_error => frame_err,
    data_in_valid       => valid_rx,
    data_in             => data_rx,

    -- data out
    data_out_valid => data_out_valid,
    data_out       => data_out
  );


  inject: entity work.uart
    generic map (
      CLK_FREQ    => 50e6,
      BAUD_RATE   => 115200,
      PARITY_BIT  => "none"
    )
    port map (
      CLK         => clk,
      RST         => rst,
      -- UART INTERFACE
      UART_TXD    => uart_txd_o,
      UART_RXD    => uart_rxd_i,
      -- USER DATA OUTPUT INTERFACE
      DATA_OUT    => data_rx,
      DATA_VLD    => valid_rx,
      FRAME_ERROR => frame_err,
      -- USER DATA INPUT INTERFACE
      DATA_IN     => data_tx,
      DATA_SEND   => wr_uart,
      BUSY        => open
    );


end tb;
