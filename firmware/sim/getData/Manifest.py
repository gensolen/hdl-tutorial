action = "simulation"
sim_tool = "modelsim"
sim_top = "getData_tb"

sim_post_cmd = "vsim -do wave.do -i getData_tb"

files = [
  "getData_tb.vhd"
]

modules = {
  "local" : [ 
    "../../modules/injector",
    "../../modules/uart_simple" 
  ]
}
