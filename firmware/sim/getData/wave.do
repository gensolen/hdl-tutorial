onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /getdata_tb/uut/clk
add wave -noupdate /getdata_tb/uut/rst
add wave -noupdate -color {Sky Blue} /getdata_tb/uut/start
add wave -noupdate /getdata_tb/uut/leds
add wave -noupdate /getdata_tb/uut/state
add wave -noupdate /getdata_tb/uut/armed
add wave -noupdate /getdata_tb/uut/ready
add wave -noupdate /getdata_tb/uut/sample_counter
add wave -noupdate /getdata_tb/uut/data_buff
add wave -noupdate -divider {data out}
add wave -noupdate /getdata_tb/uut/data_low
add wave -noupdate /getdata_tb/uut/valid_low
add wave -noupdate /getdata_tb/data_rx
add wave -noupdate /getdata_tb/valid_rx
add wave -noupdate /getdata_tb/frame_err
add wave -noupdate /getdata_tb/data_out_valid
add wave -noupdate /getdata_tb/data_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {16000010000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 253
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {17734341253 ps}
