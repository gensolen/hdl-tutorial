action = "simulation"
sim_tool = "modelsim"
sim_top = "uart_tb"

sim_post_cmd = "vsim -do wave.do -i uart_tb"

files = [
  "../../modules/uart_simple/source/uart_tb.vhd"
]

modules = {
  "local" : [ "../../modules/uart_simple" ],
}
