

library IEEE;
  use IEEE.std_logic_1164.all;
  use IEEE.numeric_std.all;


entity uart_tb is
  generic (
    FIFO_W    : positive := 2;
    bit_width : positive := 8
  );
end uart_tb;


architecture tb of uart_tb is

  signal clk      : std_logic := '0';
  signal rst      : std_logic := '1';
  signal counter  : natural   := 0;

  signal data_rx  : std_logic_vector(bit_width-1 downto 0) := (others => '0');
  signal data_tx  : std_logic_vector(bit_width-1 downto 0) := (others => '0');

  -- uart signals
  signal UART_TXD_o  : std_logic := '0';
  signal UART_RXD_i  : std_logic := '0';
  signal rd_uart     : std_logic := '0';
  signal wr_uart     : std_logic := '0';
  signal rx_empty    : std_logic := '0';
  signal tx_full     : std_logic := '0';

begin

  -- hold reset high up for 500 time steps
  rst <= '1' when counter < 500 else
         '0';


  -- loopback serial uart lines
  UART_RXD_i <= UART_TXD_o;


  -- generate a 50 MHz clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 20 ns (= 50 MHz)
    wait for 10 ns;
    clk <= '1';
    wait for 10 ns;
  end process clk_proc;


  -- generate a time counter (clock cycles)
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;


  -- unit under test
  uut: entity work.uart
  generic map (
    FIFO_W  => FIFO_W
  )
  port map (
    clk      => clk,
    reset    => rst,
    rx       => UART_RXD_i,
    tx       => UART_TXD_o,
    -- receive data
    rd_uart  => rd_uart,
    r_data   => data_rx,
    rx_empty => rx_empty,
    -- transmit data
    wr_uart  => wr_uart,
    w_data   => data_tx,
    tx_full  => tx_full
  );


  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      case counter is
        when 100000 =>
          wr_uart <= '1';
          data_tx <= x"0f";
        when 300000 =>
          wr_uart <= '1';
          data_tx <= x"f0";
        when 500000 =>
          wr_uart <= '1';
          data_tx <= x"00";
        when 700000 =>
          wr_uart <= '1';
          data_tx <= x"01";
        when 1000000 =>
         wr_uart <= '1';
         data_tx <= x"02";
        when 1200000 =>
          wr_uart <= '1';
          data_tx <= x"0a";
        when 1400000 =>
          wr_uart <= '1';
          data_tx <= x"a0";
        when 1600000 =>
          wr_uart <= '1';
          data_tx <= x"f1";
        when 1800000 =>
          wr_uart <= '1';
          data_tx <= x"ff";
        when 2000000 =>
          wr_uart <= '1';
          data_tx <= x"aa";
        when others =>
          wr_uart <= '0';
          data_tx <= (others => '0');
      end case;
    end if;
  end process sim_proc;

end tb;
