onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uart_tb/CLK
add wave -noupdate /uart_tb/RST
add wave -noupdate /uart_tb/tx_uart
add wave -noupdate /uart_tb/rx_uart
add wave -noupdate /uart_tb/data_vld
add wave -noupdate /uart_tb/data_out
add wave -noupdate /uart_tb/frame_error
add wave -noupdate /uart_tb/data_send
add wave -noupdate /uart_tb/busy
add wave -noupdate /uart_tb/data_in
add wave -noupdate /uart_tb/clk_period
add wave -noupdate /uart_tb/uart_period
add wave -noupdate /uart_tb/data_value
add wave -noupdate /uart_tb/data_value2
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3000000000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 326
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {200 us}
run 200 us
