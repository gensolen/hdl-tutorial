action = "simulation"
sim_tool = "modelsim"
sim_top = "bin_mult_tb"

sim_post_cmd = "vsim -do wave.do -i bin_mult_tb"

files = [
  "bin_mult_tb.vhd"
]

modules = {
  "local" : [ "../../modules/bin_mult" ],
}
