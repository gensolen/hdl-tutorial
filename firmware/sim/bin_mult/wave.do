onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /bin_mult_tb/clk
add wave -noupdate -color Yellow -label a -radix unsigned /bin_mult_tb/a
add wave -noupdate -color Yellow -label b -radix unsigned /bin_mult_tb/b
add wave -noupdate -color Yellow -label c -radix unsigned /bin_mult_tb/bin_mult_inst/c
add wave -noupdate -label matrix_s -expand -subitemconfig {/bin_mult_tb/bin_mult_inst/matrix_s(5) {-color Red -height 15} /bin_mult_tb/bin_mult_inst/matrix_s(4) {-color Red -height 15} /bin_mult_tb/bin_mult_inst/matrix_s(3) {-color Red -height 15} /bin_mult_tb/bin_mult_inst/matrix_s(2) {-color Red -height 15} /bin_mult_tb/bin_mult_inst/matrix_s(1) {-color Red -height 15} /bin_mult_tb/bin_mult_inst/matrix_s(0) {-color Red -height 15}} /bin_mult_tb/bin_mult_inst/matrix_s
add wave -noupdate -color Cyan -label j -radixenum symbolic /bin_mult_tb/bin_mult_inst/j
add wave -noupdate -color Cyan -label row -radixenum symbolic /bin_mult_tb/bin_mult_inst/row
add wave -noupdate -color Cyan -label c_temp -radixenum symbolic /bin_mult_tb/bin_mult_inst/c_temp
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 111
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {94500 ps}
run 90 ns
