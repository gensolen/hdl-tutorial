-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file bin_mult_tb.vhd
--! @brief Testbench, which simulates bin_mults
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity bin_mult_tb is
  generic(
    bit_length : integer := 6
  );
end bin_mult_tb;

architecture arch of bin_mult_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut (unit under test) ports
  signal a : std_logic_vector (bit_length-1 downto 0) := (others => '1');
  signal b : std_logic_vector (bit_length-1 downto 0) := "111001";

begin

  -- uut bin_mult.vhd
  bin_mult_inst: entity work.bin_mult
  port map (
    clk   => clk,
    a     => a,
    b     => b,
    c     => open
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

end arch;
