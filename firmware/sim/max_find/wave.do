onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clock /max_find_tb/clk
add wave -noupdate -color Yellow -label start_i /max_find_tb/start_i
add wave -noupdate -color Yellow -label data_i -radix decimal /max_find_tb/data_i
add wave -noupdate -color Yellow -label data_o -radix decimal /max_find_tb/data_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 96
configure wave -valuecolwidth 47
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {147 ns}
run 140 ns
