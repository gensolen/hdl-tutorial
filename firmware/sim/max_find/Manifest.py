action = "simulation"
sim_tool = "modelsim"
sim_top = "max_find_tb"

sim_post_cmd = "vsim -do wave.do -i max_find_tb"

files = [
  "max_find_tb.vhd"
]

modules = {
  "local" : [ "../../modules/max_find" ],
}
