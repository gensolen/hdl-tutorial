-- EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
-- vim: tabstop=2:shiftwidth=2:expandtab
-- kate: tab-width 2; replace-tabs on; indent-width 2;
--------------------------------------------------------------------------------
--! @file max_find_tb.vhd
--! @brief Testbench, which simulates max_find.vhd
--! @author: Philipp Horn <philipp.horn@cern.ch>
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity max_find_tb is
  -- Define generic for uut (unit under test)
  generic (
    bit_width : positive := 14
  );
end max_find_tb;

architecture tb of max_find_tb is

  -- define general signals
  signal clk      : std_logic := '0';
  signal counter  : integer := 0;

  -- define signals for uut ports
  signal start_i  : std_logic := '0';
  signal data_i   : signed(bit_width-1 downto 0) := (others => '0');
  signal data_o   : signed(bit_width-1 downto 0) := (others => '0');

begin

  -- uut max_find.vhd
  uut: entity work.max_find
  generic map (
    bit_width => bit_width
  )
  port map (
    clk     => clk,
    start_i => start_i,
    data_i  => data_i,
    data_o  => data_o
  );

  -- generate clock
  clk_proc: process
  begin
    clk <= '0';     -- clock cycle is 10 ns
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process clk_proc;

  -- generate a counter to time input
  count_proc: process(clk)
  begin
    if rising_edge(clk) then
      counter <= counter + 1;
    end if;
  end process count_proc;

  -- define input for simulation
  sim_proc: process(clk)
  begin
    if rising_edge(clk) then
      if (counter = 1) or (counter = 9) then
        start_i <= '1';
      else
        start_i <= '0';
      end if;

      case counter is
      when 2 =>
        data_i <= to_signed(12,bit_width);
      when 3 =>
        data_i <= to_signed(49,bit_width);
      when 4 =>
        data_i <= to_signed(23,bit_width);
      when 5 =>
        data_i <= to_signed(60,bit_width);

      when 9 =>
        data_i <= to_signed(80,bit_width);
      when 10 =>
        data_i <= to_signed(49,bit_width);
      when 11 =>
        data_i <= to_signed(50,bit_width);
      when 12 =>
        data_i <= to_signed(23,bit_width);

      when others =>
        data_i <= (others => '0');
      end case;
    end if;
  end process sim_proc;
end tb;
