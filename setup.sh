#!/usr/bin/env bash
#
# ==========================================================================
#
#   please set the terascale workshop software installation directory here
#
# binaries for CentOS 7.6
# export TS_INSTALL_DIR=/mnt/usbStick/centOS_7
#
# binaries for Ubuntu 16.04
export TS_INSTALL_DIR=/media/fpga//ubuntu_16
#
# local installation
# export TS_INSTALL_DIR=/opt
#
# ==========================================================================

export ALTERA=$TS_INSTALL_DIR/intel/13.1.web
export QUARTUS_ROOTDIR=$ALTERA/quartus
export QUARTUS_64BIT=1

export PATH=$PATH:$ALTERA/quartus/bin:$ALTERA/modelsim_ase/bin:$TS_INSTALL_DIR/hdlmake/3.0/bin

# needed for free 32bit modelsim version
export LD_LIBRARY_PATH=$TS_INSTALL_DIR/freetype2/32_bit/lib:$LD_LIBRARY_PATH


# ========================
#   OS specific settings
# ========================

if cat /etc/*release | grep ^NAME | grep Ubuntu; then
  echo "  *** Ubuntu OS detected ***"
  # enable mc to exit into current directory (not the one it was called from)
  mc()
  {
    MC=/tmp/mc$$-"$RANDOM"
    /usr/bin/mc -P "$MC"
    cd "`cat $MC`"
    rm "$MC"
    unset MC;
  }
elif cat /etc/*release | grep ^NAME | grep CentOS; then
  echo "  *** CentOS detected ***"
else
  echo "OS NOT KNOWN"
fi

