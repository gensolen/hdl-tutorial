#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function

import easygui

# ------------------------------
#   main
# -------------------------------

def main(argv):
  sc = slowControl()
  sc.run()


# ----------------------
#   Slow Control Class
# ----------------------

from serial.tools import list_ports
from time import time, sleep

class slowControl(object):

  def __init__(self, inputFile = None):
    self.adc = adcData(debug = None)
    self.com = None


  def printMenu(self):
    print()
    if self.adc:
      print("  (1) select ADC data input file [%s]" % self.adc.getFileName())
      print("  (2) set scaling factor [%i]" % self.adc.getScale())
      print("  (3) set pulse index [%i]" % self.adc.getIdxPulse())
    else:
      print("  (1) select ADC data input file [None]")
      print("  (2) set scaling factor [1]")
      print("  (3) set pulse index [0]")
    print("  (4) plot ADC samples")
    print()
    if self.com:
      print("  (5) open UART port [%s]" % self.com.getPort())
    else:
      print("  (5) open UART port [None]")
    if self.adc:
      print("  (6) send pulse [%i] to fpga" % self.adc.getIdxPulse())
    else:
      print("  (6) send pulse [0] to fpga")
    print()
    print("  (0) exit")
    print()


  def listPorts(self, debug = None):
    ports = list_ports.comports(include_links=False)
    if debug:
      print()
      print("  INFO: available serial ports:")
      for port in ports :
        print("    ", port.device)
    return sorted(ports)


  def run(self):
    option = 1
    while True:

      self.printMenu()

      try:
        tmp = input('  Enter option [%i]: ' % option)
        if tmp != "":           # take default value
          option = int(tmp)
      except ValueError:
        print("    WARNING: Not a valid option, enter a number please")
        continue


      if option == 0:
        if self.com:
          self.com.disconnect()
        print("  ... bye")
        print()
        break


      elif option == 1:
        inputFile = easygui.fileopenbox()
        if inputFile:
          self.inputFile = inputFile
          self.adc.setFileName(inputFile)
          self.adc.readFile()
        else:
          print("    WARNING: No file selected")


      elif option == 2:
        print("    INFO: option (2) set scaling factor")
        if self.adc:
          default = self.adc.getScale()
        else:
          default = 1
        try:
          scale = int(input('  Enter scaling factor: [%i]' % default))
          if self.adc:
            self.adc.setScale(scale)
          else:
            print("    WARNING: there is no adc data loaded, ignoring scaling factor")
        except ValueError:
          print("    WARNING: Not a valid number, number must be an integer")
          continue


      elif option == 3:
        print("    INFO: option (3) set pulse index")
        if self.adc:
          try:
            default = self.adc.getIdxPulse() + 1
            tmp  = input('  Enter pulse index [%i]: ' % default)
            if tmp == "":           # take default value
              self.adc.setIdxPulse(default)
            elif int(tmp) < 98:
              self.adc.setIdxPulse(int(tmp))
          except ValueError:
            print("    WARNING: Not a valid number")
            continue
        else:
          print("    WARNING: there is no adc data loaded, ignoring your choise")


      elif option == 4:
        print("    INFO: option (4) plot ADC samples")
        if self.adc:
          self.adc.plotPulse()
        else:
          print("    WARNING: can't plot, there is no adc data loaded")


      elif option == 5:
        print("    INFO: option (5) open UART port")
        ports = self.listPorts()
        print()
        for i, port in enumerate(ports) :
          print("  (%i) select port [%s]" % (i + 1, port.device))
        print()
        print("  (0) return")
        print()
        try:
          tmp = input('  Enter option [1]: ')
          if tmp == "9":     # do nothing
            continue
          elif tmp == "":    # take default
            port = ports[0]
          else:
            port = ports[int(tmp) - 1]

          try:
            if self.com:
              self.com.setPort(port.device)
              self.com.connect()
            else:
              self.com = com(port = port.device)
              self.com.connect()
          except  ValueError:
            print("    ERROR: Cannot open port, check connection")
            continue

        except ValueError:
          print("    WARNING: Not a valid selection, enter a number please")
          continue
        

      elif option == 6:
        print("    INFO: option (6) send pulse")
        idxPulse = self.adc.getIdxPulse()
        high, low = self.adc.getSplitedPulse(idxPulse)
        for i in range(len(high)):
          self.com.sendByte(low[i])
          sleep(self.com.getSleepTime())
          self.com.sendByte(high[i])
          sleep(self.com.getSleepTime())
        self.adc.setIdxPulse(idxPulse + 1)
        print()
        print("    INFO: done :-)")

      else:
        print("    WARNING: Not a valid option, please enter a number in range 0 to 6")


# ------------------
#   ADC Data Class
# ------------------

import matplotlib.pyplot as plt

class adcData(object):
  dummy = [
          0,     # 2**0
          2,     # 2**1
          4,     # 2**2
          8,     # 2**3
         16,     # 2**4
         32,     # 2**5
         64,     # 2**6
        128,     # 2**7
        256,     # 2**8
        512,     # 2**9
       1024,     # 2**10
       2048,     # 2**11
       4096,     # 2**12
       8192,     # 2**13 
      16384 - 1, # 2**14 - 1
      16384,     # not vaild
    ]


  def __init__(self, fName = None, bitwidth = 14, offset = 1024, debug = False):
    self.debug = debug
    self.fName = fName
    self.bitwidth = bitwidth
    self.offset = offset
    self.numSamples = 32
    self.idxPulse = 0
    self.scale = 1
    self.fig = None
    if fName:
      self.readFile()
    else:
      self.data = adcData.dummy


  def setIdxPulse(self, idxPulse):
    self.idxPulse = idxPulse


  def getIdxPulse(self):
    return self.idxPulse


  def setNumSamples(self, numSamples):
    self.numSamples = numSamples


  def getNumSamples(self):
    return self.numSamples


  def setOffset(self, offset):
    self.offset = offset


  def getOffset(self):
    return self.offset


  def setScale(self, scale):
    self.scale = scale


  def getScale(self):
    return self.scale


  def setFileName(self, fName):
    self.fName = fName


  def getFileName(self):
    return self.fName


  def getData(self):
      return self.data[:]


  def readFile(self):
    data = []
    idx = 0
    adc = 0
    try:
      with open(self.fName, mode = 'r') as f:
        for line in f:
          if line[0] != '!':
            idx, adc = line.split(' ')
            # data.append((int(float(idx)), int(float(adc)), float(filterOut)))
            data.append(int(float(adc)))
      self.data =  data
    except IOError:
      print("    ERROR: can't read file {}, loading dummy data".format(self.fName) )
      self.data = adcData.dummy
      self.fName = None


  def getPulse(self, idxPulse):
    fileOffset = 1000
    start = fileOffset + 20 + idxPulse * 100
    stop = start + self.getNumSamples()
    idxSample = range(start, stop)
    yraw = list(map(self.data.__getitem__, [x - fileOffset for x in idxSample]))
    print("    INFO: taking pulse starting at", start)
    return idxSample, [i * self.getScale() + self.getOffset() for i in yraw]
    

  def getSplitedPulse(self, idxPulse):
    idx, adc = self.getPulse(idxPulse)
    high = []
    low = []
    for i, value in enumerate(adc):
      if self.isValid(value):
        htemp, ltemp = self.splitValue(value)
        high.append(htemp)
        low.append(ltemp)
      else:
        print("    ERROR: data not valid %i" % value)
        return  None, None
    return high, low


  def plotPulse(self):
    x, y = self.getPulse(self.getIdxPulse())
    pulseMax = max(y)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.hlines(y=self.getOffset(), xmin=min(x), xmax=max(x), linewidth=0.5,
        color='k')
    line, = ax.plot(x, y, 'b-')
    points, = ax.plot(x, y, 'r.', label='Max = ' + str(pulseMax))
    ax.legend(loc='best')
    fig.canvas.draw()
    fig.show()


  def isValid(self, value):
    return (value >= 0) and (value < (2**self.bitwidth))


  def idxValid(self, idx):
    try:
      self.data[idx]
      return True
    except IndexError:
      print("    WARNING: Index is out of range")
      return False


  def getNext(self, idx):
    try: 
      self.data[idx + 1]
      return idx + 1
    except IndexError:
      print("    WARNING: Out of range, rewinding")
      return 0


  def getValue(self, idx):
    try: 
      return self.data[idx]
    except IndexError:
      print("    WARNING: Out of range, rewinding")
      return self.data[0]


  def printData(self):
    for i, value in enumerate(self.data):
      print('   ', i, value, self.isValid(value))


  def testBit(self, int_type, offset):
    mask = 1 << offset
    return(int_type & mask)


  def setBit(self, int_type, offset):
    mask = 1 << offset
    return(int_type | mask)


  def splitValueIdx(self, idx):

    halfBitWidth = self.bitwidth // 2

    high = self.data[idx] >> halfBitWidth
    high = self.setBit(high, halfBitWidth)

    low = 0
    for i in range(halfBitWidth):
      low += self.testBit(self.data[idx], i)

    if self.debug:
      print()
      print('  *** DEBUG ***')
      print('  orig', "{num:0{width}b}".format(num = self.data[idx], width = self.bitwidth))
      print('  high', "{num:0{width}b}".format(num = high, width = 8), high)
      print('  low ', "{num:0{width}b}".format(num = low, width = 8), low)

    return high, low


  def splitValue(self, value):

    halfBitWidth = self.bitwidth // 2

    high = value >> halfBitWidth
    high = self.setBit(high, halfBitWidth)

    low = 0
    for i in range(halfBitWidth):
      low += self.testBit(value, i)

    if self.debug:
      print()
      print('  *** DEBUG ***')
      print('  orig', "{num:0{width}b}".format(num = value, width = self.bitwidth))
      print('  high', "{num:0{width}b}".format(num = high, width = 8), high)
      print('  low ', "{num:0{width}b}".format(num = low, width = 8), low)

    return high, low


# ------------------------------
#   serial communication class
# ------------------------------

import serial
import struct  # needed to send byte

class com(object):

  """
    Serial port implementation
    INFO: https://github.com/swharden/pyTENMA/blob/master/pyTENMA.py

      port – Device name or None.
        e.g "/dev/ttyS0"
      baudrate (int) – Baud rate such as 9600 or 115200 etc.
        typical baudrates:
              50,    75,   110,   134, 
             150,   200,   300,   600, 
            1200,  1800,  2400,  4800,
            9600, 19200, 38400, 57600, 
          115200
      bytesize – Number of data bits. 
        Possible values: 
          FIVEBITS, 
          SIXBITS, 
          SEVENBITS, 
          EIGHTBITS
      parity – Enable parity checking. 
        Possible values: 
          PARITY_NONE, 
          PARITY_EVEN, 
          PARITY_ODD 
          PARITY_MARK, 
          PARITY_SPACE
      stopbits – rtscts = Falsen3  Number of stop bits. 
        Possible values: 
          STOPBITS_ONE, 
          STOPBITS_ONE_POINT_FIVE, 
          STOPBITS_TWO
      timeout (float) – Set a read timeout value.
      xonxoff (bool) – Enable software flow control.
      rtscts (bool) – Enable hardware (RTS/CTS) flow control.
      dsrdtr (bool) – Enable hardware (DSR/DTR) flow control.
      write_timeout (float) – Set a write timeout value.
      inter_byte_timeout (float) – Inter-character timeout, None to disable (default).
      exclusive (bool) – Set exclusive access mode (POSIX only). A port cannot be opened 
                         in exclusive access mode if it is already open in exclusive access mode.

  """

  def __init__(self, port = None):
    """
      this com class init method calls the serial init method and 
      defines in addition the variables port, startTime and sleepTime
    """
    self.port = port
    self.startTime = None
    self.sleepTime = 0.001

    self.ser = serial.Serial()   
    self.ser.port = self.port
    self.ser.baudrate = 115200
    self.ser.bytesize = serial.EIGHTBITS
    self.ser.parity = serial.PARITY_NONE
    self.ser.stopbits = serial.STOPBITS_ONE
    self.ser.timeout = 0.3
    self.ser.xonxoff = False
    self.ser.rtscts = False
    self.ser.dsrdtr = False
    self.ser.write_timeout = 0.3
    self.ser.inter_byte_timeout = 0.1
    self.ser.excluisive = True


  def setSleepTime(self, sleepTime):
    """
      set: sleepTime
      this variable might be used to pause between 
      two successive transmits
    """
    self.sleepTime = sleepTime


  def getSleepTime(self):
    """
      return: sleepTime
      this variable might be used to pause between 
      two successive transmits
    """
    return self.sleepTime


  def setPort(self, port):
    """
      set: port
      set the communication port, e.g. /dev/ttyS0
    """
    self.port = port


  def getPort(self):
    """
      return: port
      returns the communication port, e.g. /dev/ttyS0
    """
    return self.port


  def sendByte(self, byte):
    """
      send the value of the argument byte through the serial line

      INFO https://docs.python.org/2/library/struct.html
    """
    self.ser.write(struct.pack('B', byte))  # unsigned char (1 byte)


  def connect(self):
    """
      opens the communication to the defined port
    """
    try:
      self.ser.open()
      self.ser.readline() # make sure we can read (may be incomplete)
      print ("   INFO: connected to %s!" % self.port)
      self.startTime = time()

    except IOError: # if port is already opened, close it, open it again and print message
      self.ser.close()
      self.ser.open()
      print ("    WARNING: port %s was already open, it was closed and opened again!" % self.port)


  def disconnect(self):
    """
      closes the communication port
      Run this before the python script terminates. This will disconnect
      from the serial port and makes it easier to reconnect in the future.
    """
    print("    INFO: connection time was %i seconds" % self.timeConnected())
    print("    INFO: disconnecting port %s! ..." % self.port)
    self.ser.close()
    print("    INFO: all clear!")


  def timeConnected(self):
    """
      This functions returns the connection time since the port was opened
    """
    return time() - self.startTime 


import sys
if __name__ == "__main__":
  main(sys.argv[1:])


