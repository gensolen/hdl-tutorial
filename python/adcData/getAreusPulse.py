#! /usr/bin/env python

"""Example file for access to AREUS ROOT files.

This script accesses a ROOT file of your choice and iterates over two
histograms in it to show how to use ROOT files in Python.

call with e.g.
  $ python3 ts_pyroot_example.py AREUS_SingleElectrons_EMB_20_to_50GeV/of_max_analysis_part0_of1.root
"""

import sys
import ROOT


def get_objects(rootfile, names):
    """Get a list of objects from a ROOT file by name.

    `names` is an iterable of object names.
    This raises a `ValueError` if any of the given objects does not exist
    in the ROOT file.
    """
    objects = []
    for name in names:
        next_object = rootfile.Get(name)
        if not next_object:
            raise ValueError("Could not open object {!r}".format(name))
        else:
            objects.append(next_object)
    return objects


def main():
    """The main function."""
    # Access the command line.
    try:
        filepath = sys.argv[1]
    except IndexError:
        print("Usage: {} <ROOT File>".format(sys.argv[0]))
        return 1

    # open the export file
    exportFile = open("ADC_samples.txt", mode = 'w') 

    # Open the ROOT file.
    rootfile = ROOT.TFile(filepath)
    if not rootfile:
        print("Could not open file:", filepath)
        return 1
    try:
        # Read the histograms from the ROOT file.
        # You can also use TFile.Cd and all other methods to make this
        # shorter.
        h_adc = get_objects(rootfile,
          ["EMB_EMMiddle_0.5125X0.0491/0_digitization/digits_out_sequence_cnt"]
        )[0]

        # ADC data and OF data
        exportFile.write("!\n")
        exportFile.write("! sample_index ADC_data filter_output\n")
        exportFile.write("!\n")
        max_i = 0

        n_bins = h_adc.GetNbinsX()
        # Iterate over bin indices.
        for i_bin in range(1, n_bins + 1):
            # Do whatever.
            if 1000 <= i_bin <= 10000:
                exportFile.write("{} {}\n".format(i_bin, h_adc[i_bin]))
                if  h_adc[i_bin] >  h_adc[max_i]:
                    max_i = i_bin
                if h_adc[i_bin] != 0.0:
                    print(i_bin, h_adc[i_bin])
        exportFile.write("!\n")
        exportFile.write("! Maximum pulse at sample {}\n".format(max_i))

    # The `finally` block ensures that the ROOT file is always closed.
    finally:
        rootfile.Close()
    return 0


if __name__ == "__main__":
    sys.exit(main())
